# change below folders 
echo "What is your web server root directory?"
read WEB_ROOT
echo "What is your installation Folder?"
read CRM_DIR

cd $WEB_ROOT

# Logs in to MySQL and recreate database. 
echo "What is your MySQL server user?"
read MYSQL_USR
echo "What is your MySQL server password?"
read MYSQL_PWD
mysql -u${MYSQL_USR} -p"${MYSQL_PWD}" -e "drop  database if exists ${CRM_DIR}"
mysql -u${MYSQL_USR} -p"${MYSQL_PWD}" -e "create  database if not exists ${CRM_DIR}"

# Removing existing source directory and create new
rm -rf "${CRM_DIR}/"
mkdir ${CRM_DIR}
# moving source files & unzip
cp  v72terminal/v72base.zip "${CRM_DIR}/"

# Please opens config.inc.php file and change db credentials and your web root directory
cp v72terminal/config.inc.php "${CRM_DIR}/"
echo ${MYSQL_PWD} >> "${CRM_DIR}/pwd"
echo ${CRM_DIR} >> "${CRM_DIR}/dbn"


cd "${CRM_DIR}/"
unzip v72base.zip
chmod -R 777 "../${CRM_DIR}"

mysql -u${MYSQL_USR} -p"${MYSQL_PWD}" $CRM_DIR < migrations/database/base.sql





