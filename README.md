# Setting up CRM 
Below steps are used to setup the pre-built CRM for development and QA purpose of Radus28 team.
Further there training matterials to understand 

* CRM Functionalities
* Basics of development

## Linux

1. clone this repo anywhere to you local disk
2. Move the foldoer "v72terminal" into you web server root (eg, /var/www/html)
3. Open terminal, navigate to folder "v72terminal" 
4. Run "sudo sh refresh72.sh" ( may avoid "sudo" if logged in as root)
  This will ask below questions
	- "What is your web server root directory?" Eg /var/www/html (No trailing slash)
	- "What is your installation Folder?"  The folder you like to install the CRM. Eg 'v72base', then you will run CRM in browser as http://localhost/v72base/
	-  What is your MySQL server user? 
	-  What is your MySQL server password?
	
5. Once you will give all above information the installation process will start. If you will not notice any errors, then installation is success
6. Run CRM in browser like "https://<your domain>/<your installation folder>
  (user : admin,  password : CT1234ct! )
  
## Windows
Follow below steps in windows 10 or equalant Os

* Follow step 1 and 2 from above  ( no need to clone into web root)
* Move v72base.zip to your web root, then extract it
* Copy v72terminal/win/config.inc.php into the folder v72base/
* Open config.inc.php and
	  - change database credentials 
	  - Change "$dbconfig['db_name']" to "v72base"
 	  - change $site_URL (Your crm Full Url)
 	  - change $root_direcroty (Your crm Full path, eg /var/www/html/v72base)
  
* Create a database named v72base AND restore v72base/migrations/database/base.sql
* Run CRM in browser like "https://<your domain>/v72base
(You may change the directory 'v72base' to any name )
          